json.extract! quotation, :id, :author, :quote, :date, :category_id, :created_at, :updated_at
json.url quotation_url(quotation, format: :json)
