require 'csv'

class Quotation < ApplicationRecord
  belongs_to :category, optional: true
  attr_accessor :new_category

  def self.to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["Quote", "Author", "Category", "Date"]

      all.each do |quotation|
        csv << [quotation.quote, quotation.author, quotation.category.name, quotation.date]
      end
    end
  end
end
