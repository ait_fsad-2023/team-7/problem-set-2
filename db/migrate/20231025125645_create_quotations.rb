class CreateQuotations < ActiveRecord::Migration[7.0]
  def change
    create_table :quotations do |t|
      t.string :author
      t.string :quote
      t.date :date
      t.references :category, null: false, foreign_key: true

      t.timestamps
    end
  end
end
