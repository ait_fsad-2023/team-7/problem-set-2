Rails.application.routes.draw do
  resources :quotations do
    collection do
      get 'export'
    end
  end
  resources :categories
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "quotations#index"
end
