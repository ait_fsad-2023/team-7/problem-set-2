# Problem Set 2

### Submitted by: Sunil Prajapati
### ID: 124073

This repository contains the rails application for problem set 2 regarting the Quotation Database

The deployed server is available at: [http://ec2-44-193-163-141.compute-1.amazonaws.com:3000](http://ec2-44-193-163-141.compute-1.amazonaws.com:3000/)

## Prerequisites
- Ruby
- Rails
- Prostgresql

To install the all the package, please run the command:

`bundle install`

To create the database, please run the command:

`rake db:create`

To migrate the necessary tables, please run the command:

`rake db:migrate`

To start the rails server, please run the command:

`rails server`